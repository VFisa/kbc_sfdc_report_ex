"__author__ = 'Martin Fiser'"
"__credits__ = 'Keboola 2017, Twitter: @VFisa'"

"""
Python 3 environment (unicode script fixes in place for header)
https://pypi.org/project/simple-salesforce/0.74.2/

import pytz
import datetime
end = datetime.datetime.now(pytz.UTC)  # we need to use UTC as salesforce API requires this!
sf.Contact.deleted(end - datetime.timedelta(days=10), end)

"""


import logging
import os
import sys
import requests
from keboola import docker
import logging_gelf.formatters
import logging_gelf.handlers
from simple_salesforce import Salesforce
from unidecode import unidecode



# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
"""
logger = logging.getLogger('gelf')
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])
"""


# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
USERNAME = cfg.get_parameters()["username"]
PASSWORD = cfg.get_parameters()["#password"]
SECURITY_TOKEN = cfg.get_parameters()["#security_token"]
INSTANCE = cfg.get_parameters()["instance"] #ns50
REPORTS = cfg.get_parameters()["report_ids"]



def verify_params():
    """
    At this point it only outputs the list of reports.
    """

    # username, password,token, instance validation
    if len(USERNAME)==0:
        logging.error("No username specified. Exit.")
        sys.exit(1)
    if len(PASSWORD)==0:
        logging.error("No password specified. Exit.")
        sys.exit(1)
    if len(SECURITY_TOKEN)==0:
        logging.error("No token specified. Exit.")
        sys.exit(1)
    if len(INSTANCE)==0:
        logging.error("No instance specified. Exit.")
        sys.exit(1)

    # report list
    if len(REPORTS)==0:
        logging.error("No report requested. Exit.")
        sys.exit(1)
    else:
        for r in REPORTS:
            if len(r)==0:
                logging.warning("Report requested without ID filled. Skipping.")
            else:
                logging.info("Report requested: "+str(r))

    pass


def save_table(report, received_text):
    """
    Separate function for saving report.
     - Header first since it gets special unidecode treatment
     - Data rows
    """

    file = "/data/out/tables/"+str(report)+".csv"
    #print(file)

    received_text = received_text.split("\n\n\n")[0]
    received_text = received_text.split("\n")

    # header name fix - kbc header limitation
    header = received_text[0]
    header = unidecode(header)
    received_text = received_text[1:]
    logging.info("File received with this header: "+str(header))

    with open(file, 'w') as file_out:
        # header first
        file_out.write("%s\n" % header)
        # data rows
        for item in received_text:
            file_out.write("%s\n" % item)

    logging.info("Report file "+file+" produced.")
    file_out.close()

    pass


def send_request(USERNAME, PASSWORD, SECURITY_TOKEN, INSTANCE, REPORTS):
    """
    Main API request
     - Create session.
     - Send request to SFDC API.
     - Data received as strings.
     - Call function for saving it as CSV
    """
    
    # create session
    session = requests.Session()
    try:
        sf = Salesforce(username=USERNAME, password=PASSWORD, security_token=SECURITY_TOKEN, client_id='Keboola',session=session)
        logging.info("Connected.")
    except:
        logging.error("Could not log in! Please check credentials")
        sys.exit(0)
    else:
        sid = sf.session_id

    # request reports
    for report in REPORTS:
        logging.info("Downloading report: "+str(report))
        received_text = ""
        base_url = ("https://"+str(INSTANCE)+".salesforce.com/"+str(report)+"?view=d&snip&export=1&enc=UTF-8&xf=csv")
        #print(base_url)

        response = requests.get(base_url, headers = sf.headers, cookies = {'sid' : sid})

        if response.status_code == 400:
            logging.error("Bad request, response code 400.")
            sys.exit(1)
        elif response.status_code == 404:
            logging.error("Bad authentication, response code 404.")
            sys.exit(1)
        elif response.status_code == 500:
            logging.warning("Server error, response code: 200.")
            sys.exit(2)
        elif response.status_code == 200:
            logging.info("Received correct response code: 200.")
            received_text = response.text
        else:
            logging.info("API request response: "+str(response.status_code))
            received_text = ""

        print(response.text)

        # separate funtion to save data CSV file
        save_table(report, received_text)

        logging.info("Amount of records received in report "+str(report)+": "+str(len(received_text)))



if __name__ == "__main__":
    """
    Main execution script.
    """

    # Validate parameters
    verify_params()

    # Call API
    send_request(USERNAME, PASSWORD, SECURITY_TOKEN, INSTANCE, REPORTS)

    logging.info("Script completed.")
