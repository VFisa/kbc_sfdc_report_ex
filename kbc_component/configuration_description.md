### Component Configuration ###

#### Security token
Your security token isn’t displayed in your settings or profile. If your admin assigned you the “Two-Factor Authentication for API Logins” permission, use the code generated by an authenticator app, such as Salesforce Authenticator, for the security token value.
 - From your personal settings, enter Reset in the Quick Find box, then select Reset My Security Token.
 - Click Reset Security Token. The new security token is sent to the email address in your Salesforce personal settings.
 - A new security token is emailed to you when you reset your password. Or you can reset your token separately. 
 - [More information] (https://help.salesforce.com/articleView?id=user_security_token.htm)

#### Output Table mapping
Please map the output table with the same name as your report, otherwise the app has no way to figure out which report is mapped on which output table. 
