# Terms and Conditions

Salesforce Reports Extractor component for KBC is offered as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to extract data from Salesforce Reports and
Dashboards REST API to Keboola Connection Platform (KBC). 
API call is sent alongside of username, password and security token for API authentication, no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## FAQ
[Official website](https://resources.docs.salesforce.com/sfdc/pdf/salesforce_analytics_rest_api.pdf)
[security token] (https://help.salesforce.com/articleView?id=user_security_token.htm)

## Contact
Fisa    
Martin Fiser  
Vancouver, Canada (PST time)   
email: VFisa.stuff@gmail.com   