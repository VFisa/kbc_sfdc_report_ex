# Keboola Connection component - Salesforce Reports Extractor

# How-to use it locally:

## Create folder for *DATA*:
```
mkdir -p data/{in,out}/{tables,files}
```

## Config file
 1. Use *config_template.json*
 2. Fill the credentials and parameters
 3. rename *config_template.json* to *config.json*
 4. move *config.json* to the *DATA* folder

## Build Image
```
docker build -f Dockerfile -t kbc_sfdc_report_ex:latest .
```

## Run from terminal
Alter this script, specify there the location of the *REPOSITORY* and the *DATA* folder:
```
docker run -e "KBC_LOGGER_ADDR=127.0.0.1" -e "KBC_LOGGER_PORT=9401" --volume DATA/kbc_sfdc_report_ex/data:/data --volume REPOSITORY/kbc_sfdc_report_ex:/code kbc_sfdc_report_ex:latest
```

Example:
```
docker run -e "KBC_LOGGER_ADDR=127.0.0.1" -e "KBC_LOGGER_PORT=9401" --volume /Users/martin/Dropbox/Development/kbc_data/kbc_sfdc_report_ex/data:/data --volume /Users/martin/Dropbox/Development/kbc_sfdc_report_ex:/code kbc_sfdc_report_ex:latest
```

## Running it with full logging in GELF
```docker-compose up```